pipeline {
agent any

    // Define variables for checkmarx usage        
    environment {
        PROJECT_NAME = 'WebGoat'
        FILTER_EXCLUSIONS = getExclusions()
    }  

    stages {
        
        stage('Clean workspace') {
            steps {
                cleanWs disableDeferredWipeout: true
            }
        }
        
        stage('Get source') {
            steps {
                git 'https://costate@bitbucket.org/costate/webgoat.git'
            }
        }       
        
        stage('SCA Resolver scan') {
            steps {
                withCredentials([usernamePassword(credentialsId: 'SCA_JC', usernameVariable: 'SCA_USERNAME', passwordVariable: 'SCA_PASSWORD'),string(credentialsId: 'SCA_TENANT', variable: 'SCA_TENANT')]) {
                      sh '/checkmarx/scaresolver/ScaResolver -a ${SCA_TENANT} -u ${SCA_USERNAME} -p ${SCA_PASSWORD} -s "${WORKSPACE}" -n "${PROJECT_NAME}" --bypass-exitcode --report-extension Pdf --report-type Risk --report-path "${WORKSPACE}"'
                }
            }
        }
            
        stage('SCA Report'){
            steps{
                sh 'mv -T SCA_RiskReport_* SCAReport.pdf'
                publishHTML([allowMissing: false, alwaysLinkToLastBuild: true, keepAll: true, reportDir: '.', reportFiles: 'SCAReport.pdf', reportName: 'SCA Report', reportTitles: ''])
            }
        }            

    }
}


@NonCPS
def getExclusions() {
    def list = "!**/_cvs/**/*, !**/.svn/**/*, !**/.hg/**/*, !**/.git/**/*, !**/.bzr/**/*,\
        !**/.gitgnore/**/*, !**/.gradle/**/*, !**/.checkstyle/**/*, !**/.classpath/**/*,\
        !**/bin/**/*,!**/obj/**/*, !**/backup/**/*, !**/.idea/**/*, !**/*.DS_Store,!**/*.ipr, !**/*.iws,\
        !**/*.bak, !**/*.tmp, !**/*.aac, !**/*.aif, !**/*.iff, !**/*.m3u, !**/*.mid, !**/*.mp3,\
        !**/*.mpa, !**/*.ra, !**/*.wav, !**/*.wma, !**/*.3g2, !**/*.3gp, !**/*.asf, !**/*.asx,\
        !**/*.avi, !**/*.flv, !**/*.mov, !**/*.mp4, !**/*.mpg, !**/*.rm, !**/*.swf, !**/*.vob,\
        !**/*.wmv, !**/*.bmp, !**/*.gif, !**/*.jpg, !**/*.png, !**/*.psd, !**/*.tif, !**/*.swf,\
        !**/*.jar, !**/*.zip, !**/*.rar, !**/*.exe, !**/*.dll, !**/*.pdb, !**/*.7z, !**/*.gz,\
        !**/*.tar.gz, !**/*.tar, !**/*.gz, !**/*.ahtm, !**/*.ahtml, !**/*.fhtml, !**/*.hdm,\
        !**/*.hdml, !**/*.hsql, !**/*.ht, !**/*.hta, !**/*.htc, !**/*.htd, !**/*.war, !**/*.ear,\
        !**/*.htmls, !**/*.ihtml, !**/*.mht, !**/*.mhtm, !**/*.mhtml, !**/*.ssi, !**/*.stm,\
        !**/*.bin,!**/*.lock,!**/*.svg,!**/*.obj, !**/*.stml, !**/*.ttml, !**/*.txn, !**/*.xhtm,\
        !**/*.xhtml, !**/*.xml, !**/*.class, !**/*.iml, !Checkmarx/Reports/*.*, !OSADependencies.json,\
        !**/acceptance-test/**/*, !**/test/**/*, !**/OCIC/**/*, !**/mocks/**/*, !test.gradle, !coverage.gradle,\
        !**/node_modules/**/*, !**/junitResults/**/*, !**/.scannerwork/**/*, !**/coverage/**/*, !**/jmeter/**/*,\
        !dependency-check-report.html, !dependency-check-report.xml,\
        !**/*.map, !**/*.txt, !**/*.ttf, !**/*.map, !**/*.eot, !**/*.doc, !Jenkinsfile, !README.MD, !.gitignore"
    return list;
}